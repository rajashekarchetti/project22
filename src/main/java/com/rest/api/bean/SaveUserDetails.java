package com.rest.api.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonPropertyOrder({"userId","userName","firstName","lastName","salary"})
public class SaveUserDetails {

	@ApiModelProperty(value = "USER_ID",position = 0)
	@JsonProperty("USER_ID")
	private long userId;
	
	@ApiModelProperty(value = "USER_NAME",position = 1)
	@JsonProperty("USER_NAME")
	private String userName;
	
	@ApiModelProperty(value = "FIRST_NAME",position = 2)
	@JsonProperty("FIRST_NAME")
	private String firstName;
	
	@ApiModelProperty(value = "LAST_NAME",position = 3)
	@JsonProperty("LAST_NAME")
	private String lastName;
	
	@ApiModelProperty(value = "SALARY",position = 3)
	@JsonProperty("SALARY")
	private int salary;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	
	
	
}
