FROM openjdk:8
VOLUME /tmp
EXPOSE 8080
ADD target/restapi-0.0.1-SNAPSHOT.jar restapi-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/restapi-0.0.1-SNAPSHOT.jar"]